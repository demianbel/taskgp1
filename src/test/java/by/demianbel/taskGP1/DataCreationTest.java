package by.demianbel.taskGP1;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Random;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class DataCreationTest extends AbstractUITest {
	private final String[] hotelData = new String[]{
			"Vientiane Star Hotel;2;https://www.booking.com/hotel/la/vientiane-star.en-gb.html;18 Manthatourath Road, Xien Yeun Village Chanthabouly District, 01000 Vientiane, Laos",
			"Renaissance Minsk Hotel;5;https://www.booking.com/hotel/by/renaissance-minsk.en-gb.html;Prospect Dzerzhinskogo 1E, Moskovsky District, 220036 Minsk, Belarus",
			"Victoria Olimp Hotel Minsk;4;https://www.booking.com/hotel/by/viktoriia-olimp.en-gb.html;Prospect Pobediteley 103, Tsentralny District, 220020 Minsk, Belarus" 
	};
	
	private final String[] categoryData = new String[]{"Inn", "Family hotel", "Roadhouse"};
	
	@Test
	public void createData() throws InterruptedException{
		
		driver.get(BASE_URL);
        
		//Loading page
		Thread.sleep(1000); 
		
		//go to category page		
		WebElement categoryMenu = driver.findElement(By.xpath("//span[@class='v-menubar-menuitem'][./span/@class='v-menubar-menuitem-caption'][contains(./span/text(), 'Categories List')]"));
		categoryMenu.click();
		
		//Loading page
		Thread.sleep(1000); 
				
		Assert.assertEquals("http://localhost:8080/#!categories", driver.getCurrentUrl());
		for(String category:categoryData){
			createCategoryData(category);
		}
		
		//Let the user actually see something!
		Thread.sleep(1000);
		
	    //go to hotel page
		WebElement hotelMenu = driver.findElement(By.xpath("//span[@class='v-menubar-menuitem'][./span/@class='v-menubar-menuitem-caption'][contains(./span/text(), 'Hotel List')]"));
		hotelMenu.click();
		
		//Loading page
		Thread.sleep(1000);
		
		Assert.assertEquals("http://localhost:8080/#!hotels", driver.getCurrentUrl());
		for(String hotel:hotelData){
			createHotelData(hotel);
		}
	}
	
	private void createCategoryData(String category) throws InterruptedException {
		
		//Loading page
	    Thread.sleep(1000); 
	    
		WebElement newCategoryButton = driver.findElement(By.id("newCategoryBtn"));
		newCategoryButton.click();
		
		//Let the user actually see something!
		Thread.sleep(1000);
		
		WebElement categoryName = driver.findElement(By.id("categoryName"));
		WebElement saveButton = driver.findElement(By.id("saveCategoryBtn"));
		
		categoryName.sendKeys(category);
		
		//Let the user actually see something!
		Thread.sleep(1000);
		
		saveButton.click();
		
		//Let the user actually see something!
				Thread.sleep(1000);
		
		
	
	}

	public void createHotelData(String hotel) throws InterruptedException{
		
		//Loading page
		Thread.sleep(1000); 
		
		WebElement newHotelButton = driver.findElement(By.id("newHotelBtn"));
	
		newHotelButton.click();
		
		//Let the user actually see something!
		Thread.sleep(1000);
		
		//init all elements
		WebElement hotelName = driver.findElement(By.id("hotelName"));
		WebElement hotelRating = driver.findElement(By.id("hotelRating"));
		WebElement hotelCategory = driver.findElement(By.xpath("//*[@id='hotelCategory']/select"));
		Select categorySelect = new Select(hotelCategory);
		WebElement hotelAddress = driver.findElement(By.id("hotelAddress"));
		WebElement hotelOperatesFrom = driver.findElement(By.xpath("//*[@id='hotelOperatesFrom']/input"));
		WebElement hotelUrl = driver.findElement(By.id("hotelUrl"));
		
		WebElement saveButton = driver.findElement(By.id("saveBtn"));
		
		Random r = new Random();
	        
		    //set value to form
			String[] split = hotel.split(";");
			hotelName.sendKeys(split[0]);
			hotelRating.clear();
			hotelRating.sendKeys(split[1]);			
			int size = categorySelect.getOptions().size();
			categorySelect.selectByIndex(r.nextInt(size-1)+1);
			hotelAddress.sendKeys(split[3]);
			int daysOld = 0 - r.nextInt(365 * 30);
			hotelOperatesFrom.clear();
			hotelOperatesFrom.sendKeys((LocalDate.now().plusDays(daysOld).format(DateTimeFormatter.ofPattern("dd.MM.uu"))));
			hotelUrl.sendKeys(split[2]);
		
			//Let the user actually see something!
			Thread.sleep(3000);
			
			saveButton.click();
			
			//Let the user actually see something!
			Thread.sleep(1000);

	}

}
