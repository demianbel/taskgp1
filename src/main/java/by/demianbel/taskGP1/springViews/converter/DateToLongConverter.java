package by.demianbel.taskGP1.springViews.converter;

import java.time.Duration;
import java.time.LocalDate;

import com.vaadin.data.Converter;
import com.vaadin.data.Result;
import com.vaadin.data.ValueContext;

@SuppressWarnings("serial")
public class DateToLongConverter implements Converter<LocalDate, Long> {

	@Override
	public Result<Long> convertToModel(LocalDate value, ValueContext context) {
		//convert input LocalData to amount of days from opening to now
		if(value != null) {
			Long duration = Duration.between(value.atTime(0, 0), LocalDate.now().atTime(0, 0)).toDays();
			return Result.ok(duration);
		} else
			return Result.ok(null);
		
	}

	@Override
	public LocalDate convertToPresentation(Long value, ValueContext context) {
		//convert amount of days from opening to now to LocalData
		if(value != null) {
			return LocalDate.now().minusDays(value);
		} else return LocalDate.now();
	}
	
}
