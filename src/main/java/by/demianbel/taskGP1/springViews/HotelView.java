package by.demianbel.taskGP1.springViews;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.Column;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.PopupView;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.components.grid.MultiSelectionModel;
import com.vaadin.ui.renderers.HtmlRenderer;
import com.vaadin.ui.themes.ValoTheme;

import by.demianbel.taskGP1.backend.entity.Hotel;
import by.demianbel.taskGP1.backend.services.HotelService;
import by.demianbel.taskGP1.springViews.form.BulkUpdateHotelView;
import by.demianbel.taskGP1.springViews.form.HotelForm;

@SuppressWarnings("serial")
@SpringView(name = HotelView.VIEW_NAME)
public class HotelView extends VerticalLayout implements View {
	public static final String VIEW_NAME = "hotels";
 
	public HotelView(){
		
	}
	
	@Autowired
	private HotelService service;
	private Grid<Hotel> grid = new Grid<>(Hotel.class);
	private TextField filterByNameText = new TextField();
	private TextField filterByAddressText = new TextField();
	private HotelForm form; 
	private BulkUpdateHotelView bulkUpdateHotelLayout;
	private PopupView bulkEditView;
	
	@PostConstruct
	public void init(){
        //initialize form with service
		form = new HotelForm(this, service);
        bulkUpdateHotelLayout = new BulkUpdateHotelView(this, service);
        bulkEditView = new PopupView(null, bulkUpdateHotelLayout);
        
    	//components for filter by name
        filterByNameText.setPlaceholder("filter by Name...");
        filterByNameText.addValueChangeListener(e -> updateHotelList());
        filterByNameText.setValueChangeMode(ValueChangeMode.LAZY);
        
        //create button for clearing name field
        Button clearFilterByNameTextBtn = new Button(VaadinIcons.ERASER);
        clearFilterByNameTextBtn.setDescription("Clear the current filter");
        clearFilterByNameTextBtn.addClickListener(e -> filterByNameText.clear());
        
        //view settings and styling button
        CssLayout filteringByName = new CssLayout();
        filteringByName.addComponents(filterByNameText, clearFilterByNameTextBtn);
        filteringByName.setStyleName(ValoTheme.LAYOUT_COMPONENT_GROUP);
        
        //components for filter by address
        filterByAddressText.setPlaceholder("filter by Address...");
        filterByAddressText.addValueChangeListener(e -> updateHotelList());
        filterByAddressText.setValueChangeMode(ValueChangeMode.LAZY);
        
        //create button for clearing address field
        Button clearFilterByAddressTextBtn = new Button(VaadinIcons.ERASER);
        clearFilterByAddressTextBtn.setDescription("Clear the current filter");
        clearFilterByAddressTextBtn.addClickListener(e -> filterByAddressText.clear());
        
        //view settings and styling button
        CssLayout filteringByAddress = new CssLayout();
        filteringByAddress.addComponents(filterByAddressText, clearFilterByAddressTextBtn);
        filteringByAddress.setStyleName(ValoTheme.LAYOUT_COMPONENT_GROUP);
        
      //create buttons    	
    	//button for creating new hotel
    	Button newButton = new Button("New");
    	newButton.addClickListener(e -> {
    		form.closeForm();
    		grid.asMultiSelect().clear();
    		form.setHotel(new Hotel());
    	});
        
    	//Edit button for single select
    	Button edit = new Button("Edit");
    	edit.setEnabled(false);
        edit.addClickListener(e -> {
        	Set<Hotel> items = grid.asMultiSelect().getSelectedItems();
        	form.setHotel((Hotel)items.iterator().next());
        });
        
        //"Delete" button with multiSelect 
        Button delete = new Button("Delete");
        delete.setEnabled(false);
        delete.addClickListener(e -> {
        	Set<Hotel> hotels = grid.asMultiSelect().getSelectedItems();
        	Iterator<Hotel> hotelIterator = hotels.iterator();
        	while(hotelIterator.hasNext()){
        		service.delete(hotelIterator.next());
        	}
        	updateHotelList();
        });
        
       //"Bulk edit" button with multiSelect 
        Button bulkUpdate = new Button("Bulk edit");
        bulkUpdate.setEnabled(false);
        bulkUpdate.addClickListener(e -> {
        	bulkUpdateHotelLayout.setHotels(grid.asMultiSelect().getSelectedItems());
        	bulkEditView.setPopupVisible(true);
        });
        
        //set id to all buttons
        newButton.setId("newHotelBtn");
        edit.setId("editHotelBtn");
        delete.setId("deleteHotelBtn");
        bulkUpdate.setId("bulkUpdateHotelBtn");
        
     	
        //deactivating and activating buttons 
        MultiSelectionModel<Hotel> selectionModel = (MultiSelectionModel<Hotel>) grid.setSelectionMode(SelectionMode.MULTI);
        selectionModel.addMultiSelectionListener(v -> {
    		Set<Hotel> selectItems = selectionModel.getSelectedItems();
    		int size = selectItems.size();
    		form.closeForm();
    		edit.setEnabled(size == 1);
			delete.setEnabled(size != 0);
			bulkUpdate.setEnabled(size > 1);
    	});
                
        //settings of grid columns
        grid.setColumns("name", "rating", "category", "address", "operatesFrom", "description");
        Column<Hotel, String> htmlColumn = grid.addColumn(hotel -> 
                  "<a href='" + hotel.getUrl() + "' target='_blank'>Go to site -></a>",
        	      new HtmlRenderer());
        htmlColumn.setCaption("Link");
        
        //view settings
        HorizontalLayout buttons = new HorizontalLayout(newButton, edit, delete, bulkUpdate);
        HorizontalLayout main = new HorizontalLayout(grid, form);
        
        updateHotelList();
        
        //hiding form        
        form.setVisible(false);
        main.setSizeFull();
        grid.setSizeFull();
        main.setExpandRatio(grid, 1);
        bulkEditView.setSizeFull();
        bulkEditView.setHideOnMouseOut(false);
        HorizontalLayout toolbar = new HorizontalLayout(filteringByName, filteringByAddress);
        addComponents(toolbar, buttons, main, bulkEditView);
	}
	
	 public void updateHotelList() {
			List<Hotel> hotels = service.findAll(filterByNameText.getValue(), filterByAddressText.getValue());
	    	grid.setItems(hotels);
		}
	
	@Override
	public void enter(ViewChangeEvent event) {
		// This view is constructed in the init() method()
	}

}
