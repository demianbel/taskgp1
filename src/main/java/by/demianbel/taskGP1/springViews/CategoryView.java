package by.demianbel.taskGP1.springViews;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.components.grid.MultiSelectionModel;

import by.demianbel.taskGP1.backend.entity.Category;
import by.demianbel.taskGP1.backend.services.HotelService;
import by.demianbel.taskGP1.springViews.form.CategoryForm;

@SuppressWarnings("serial")
@SpringView(name = CategoryView.VIEW_NAME)
public class CategoryView extends VerticalLayout implements View {
	public static final String VIEW_NAME = "categories";
	
	@Autowired
	private HotelService service;
	
	private Grid<Category> categoryGrid = new Grid<>(Category.class);
	private CategoryForm categoryForm;

	@PostConstruct
	public void init(){
		//create buttons    	
    	//button for creating new category
    	Button newButton = new Button("New");
    	newButton.addClickListener(e -> {
    		categoryGrid.asMultiSelect().clear();
    		categoryForm.setCategory(new Category());
    	});
        
    	//Edit button for single select
    	Button edit = new Button("Edit");
    	edit.setEnabled(false);
        edit.addClickListener(e -> {
        	Set<Category> items = categoryGrid.asMultiSelect().getSelectedItems();
        	categoryForm.setCategory((Category)items.iterator().next());
        });
        
        //"Delete" button with multiSelect 
        Button delete = new Button("Delete");
        delete.setEnabled(false);
        delete.addClickListener(e -> {
        	Set<Category> items = categoryGrid.asMultiSelect().getSelectedItems();
        	//and set "UNDEFINED" category for hotels with deleted categories
        	service.removeCategories(items);
        	updateCategoryList();
        });
        
        //set id to buttons
        newButton.setId("newCategoryBtn");
        edit.setId("editCategoryBtn");
        delete.setId("deleteCategoryBtn");
        
     	
        //deactivating and activating buttons 
        MultiSelectionModel<Category> selectionModel = (MultiSelectionModel<Category>) categoryGrid.setSelectionMode(SelectionMode.MULTI);
        selectionModel.addMultiSelectionListener(v -> {
    		Set<Category> selectItems = selectionModel.getSelectedItems();
    		int size = selectItems.size();
    		edit.setEnabled(size == 1);
			delete.setEnabled(size != 0);	
    	});
        
        //view settings
    	HorizontalLayout buttons = new HorizontalLayout();
    	HorizontalLayout categoryMain = new HorizontalLayout();
    	
    	updateCategoryList();	
    	
    	categoryForm = new CategoryForm(this, service);
    	categoryForm.setVisible(false);
        buttons.addComponents(newButton, edit, delete);
    	categoryMain.addComponents(categoryGrid, categoryForm);
    	addComponents(buttons, categoryMain);	
	}
	
	 public void updateCategoryList(){
	    	List<Category> categories = service.getCategoriesList();
	    	categoryGrid.setItems(categories);
	        }
	
	@Override
	public void enter(ViewChangeEvent event) {
		// This view is constructed in the init() method()
	}

}
