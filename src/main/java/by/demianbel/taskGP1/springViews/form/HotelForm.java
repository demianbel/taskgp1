package by.demianbel.taskGP1.springViews.form;

import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.data.converter.StringToIntegerConverter;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.ui.Button;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;

import by.demianbel.taskGP1.backend.entity.Category;
import by.demianbel.taskGP1.backend.entity.GuarantyFee;
import by.demianbel.taskGP1.backend.entity.Hotel;
import by.demianbel.taskGP1.backend.services.HotelService;
import by.demianbel.taskGP1.springViews.HotelView;
import by.demianbel.taskGP1.springViews.converter.DateToLongConverter;

@SuppressWarnings("serial")
public class HotelForm extends FormLayout {
	
	private TextField name = new TextField("Hotel name:");
	private TextField address = new TextField("address:");
	private TextField rating = new TextField("rating:");
	private GuarantyFeeField guarantyFee = new GuarantyFeeField();
	private TextArea description = new TextArea("description:");
	private TextField url = new TextField("URL:");
	private NativeSelect<Category> category = new NativeSelect<>("category:");
	private DateField operatesFrom = new DateField("operates from:");
	private Button save = new Button("Save");
    private Button cancel = new Button("Cancel");
    
    private HotelService service;
    private Hotel hotel;
    private HotelView myView;
    private Binder<Hotel> binder = new Binder<>(Hotel.class);
	
    
    public HotelForm(HotelView myView, HotelService service) {
		this.myView = myView;
		this.service = service;
		
		setSizeUndefined();
        HorizontalLayout buttons = new HorizontalLayout(save, cancel);
        addComponents(name, rating, category, guarantyFee, address, operatesFrom, description, url, buttons);
        
        //describe all fields
        name.setDescription("name of hotel");
        address.setDescription("address of hotel");
        rating.setDescription("rating in stars from 1 to 5");
        description.setDescription("something about hotel");
        url.setDescription("link to page about hotel");
        category.setDescription("category of hotel");
        operatesFrom.setDescription("opening date");
        guarantyFee.setDescription("Payment method");
        guarantyFee.setCaption("Payment method");
        
        //set Id to all elements on form
        name.setId("hotelName");
        address.setId("hotelAddress");
        rating.setId("hotelRating");
        description.setId("hotelDescription");
        url.setId("hotelUrl");
        category.setId("hotelCategory");
        operatesFrom.setId("hotelOperatesFrom");
        guarantyFee.setId("hotelGuarantyFee");
        save.setId("saveBtn");
          
        
        category.setItemCaptionGenerator(Category::getName);
        save.setStyleName(ValoTheme.BUTTON_PRIMARY);
        save.setClickShortcut(KeyCode.ENTER);

        //bind form with Hotel fields
        binder.forField(name).asRequired("field can not be empty")
           .withValidator(v -> (v.length() <= 255),"value size must be lesser then 256")
           .bind(Hotel::getName, Hotel::setName);
        binder.forField(address).asRequired("field can not be empty")
           .withValidator(v -> (v.length() <= 255),"value size must be lesser then 256")
           .bind(Hotel::getAddress, Hotel::setAddress);
        binder.forField(rating).withConverter(new StringToIntegerConverter("value must be number from 1 to 5"))
           .asRequired("field can not be empty")
           .withValidator(v -> (v > 0),"value must be positive")
           .withValidator(v -> (v <= 5), "value most be from 1 to 5")
           .bind(Hotel::getRating, Hotel::setRating);
        binder.forField(description).bind(Hotel::getDescription, Hotel::setDescription);
        binder.forField(url).asRequired("field can not be empty")
           .withValidator(v -> (v.length() <= 255),"value size must be lesser then 255")
           .bind(Hotel::getUrl, Hotel::setUrl);
        binder.forField(category).asRequired("field can not be empty")
           .bind(Hotel::getCategory, Hotel::setCategory);
        binder.forField(operatesFrom).asRequired("field can not be empty")
           .withConverter(new DateToLongConverter())
           .withValidator(v -> (v >= 0), "please, add the date before today")
           .bind(Hotel::getOperatesFrom, Hotel::setOperatesFrom);
        binder.forField(guarantyFee).withValidator(v -> (v == null || (v.getGuarantyFee() >= 0 && v.getGuarantyFee() <=100)), "please, input number from 0 to 100")
           .bind(Hotel::getGuarantyFee, Hotel::setGuarantyFee);
        
        //activate button "save" only if all fields are correct
        binder.addStatusChangeListener(e -> {
        	boolean isValid = e.getBinder().isValid();
        	save.setEnabled(isValid);
        });
        guarantyFee.addValueChangeListener(v -> {
        	GuarantyFee oldValue = v.getOldValue();
        	GuarantyFee value = v.getValue();
        	String addition = binder.isValid()?"valid":
        		             ("not valid" + System.lineSeparator() + "Please, input number from 0 to 100");
        	String oldValueDescription = oldValue == null?"Cash":("Credit Card" + oldValue.getGuarantyFee()+ "% ");
            String newValueDescription = value == null?"Cash":("Credit Card" + value.getGuarantyFee()+ "% ");
            Notification.show("Value was changed:" + System.lineSeparator() +
            		           oldValueDescription + " --> " + newValueDescription + " !" +
            		           System.lineSeparator() + "and value is " + addition);
        });
               
        //set actions on buttons
        save.addClickListener(e -> this.save());
        cancel.addClickListener(e -> this.closeForm());
	}
    
    public void setHotel(Hotel hotel){
    	this.hotel = hotel;
    	if(hotel.getCategory() == null) category.setSelectedItem(null);
    	category.setItems(service.getCategoriesList());
    	binder.readBean(hotel);
    	setVisible(true);
    	name.selectAll();
    }
    
    public void closeForm(){
    	setVisible(false);
    	hotel = null;
    	binder.removeBean(); 	
    }
	
    private void save(){
       	try {
			binder.writeBean(hotel);
		} catch (ValidationException e) {
			Notification.show("check your fields");
		}
    	service.save(hotel);
       	myView.updateHotelList();
    	setVisible(false);
    }

}
