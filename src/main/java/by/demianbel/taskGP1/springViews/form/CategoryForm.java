package by.demianbel.taskGP1.springViews.form;

import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;

import by.demianbel.taskGP1.backend.entity.Category;
import by.demianbel.taskGP1.backend.services.HotelService;
import by.demianbel.taskGP1.springViews.CategoryView;

@SuppressWarnings("serial")
public class CategoryForm extends FormLayout{
	
	private TextField name = new TextField("Category:");
	
	private HotelService service;
    private Category category;
    private CategoryView categoryView;
    private Binder<Category> binder = new Binder<>(Category.class);
    private Button save = new Button("Save");
    private Button cancel = new Button("Cancel");
    private HorizontalLayout buttons = new HorizontalLayout(save, cancel);

    
    public CategoryForm(CategoryView categoryView, HotelService service){
    	
    	//set Id to elements
    	name.setId("categoryName");
    	save.setId("saveCategoryBtn");
    	cancel.setId("cancelCategoryEditBtn");
    	
    	this.service = service;
    	this.categoryView = categoryView;
    	setSizeUndefined();
    	addComponents(name, buttons);
    	
    	//bind fields with class
    	binder.forField(name).asRequired("value can not be empty")
    	.bind(Category::getName, Category::setName);
    	
    	//save button settings
    	save.addClickListener(e -> this.save());
    	save.setStyleName(ValoTheme.BUTTON_PRIMARY);
    	save.setClickShortcut(KeyCode.ENTER);
    	
    	//cancel button settings
    	cancel.addClickListener(e -> {
    		setVisible(false);
    		binder.removeBean();
    		
    	});
    	
    	//activate button "save" only if all fields are correct
        binder.addStatusChangeListener(e -> {
        	boolean isValid = e.getBinder().isValid();
        	save.setEnabled(isValid);
        });
        
	}

    //bind form with category and make it visible
    public void setCategory(Category category){
    	this.category = category;
    	binder.readBean(category);
    	setVisible(true);
    }
	
    //save data from form
    private void save() {
		try {
			binder.writeBean(category);
		} catch (ValidationException e) {
			Notification.show("check your fields");
		}
		service.save(category);
	
		categoryView.updateCategoryList();
		setVisible(false);
	}

}
