package by.demianbel.taskGP1.springViews.form;

import java.lang.reflect.Field;
import java.time.Duration;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Optional;
import java.util.Set;

import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import by.demianbel.taskGP1.backend.entity.Category;
import by.demianbel.taskGP1.backend.entity.Hotel;
import by.demianbel.taskGP1.backend.services.HotelService;
import by.demianbel.taskGP1.springViews.HotelView;

@SuppressWarnings("serial")
public class BulkUpdateHotelView extends Panel{

	private HotelService service;
	
	private Button update = new Button("Update");
    private Button cancel = new Button("Cancel");
	private HorizontalLayout buttons = new HorizontalLayout(update, cancel);
	private ComboBox<Field> fields = new ComboBox<>();
	private TextField newValue = new TextField();
	private ComboBox<Category> category = new ComboBox<>();
	private DateField operatesFrom = new DateField();
	VerticalLayout form = new VerticalLayout(fields, newValue, category, operatesFrom, buttons); 
	Set<Hotel> selectedHotels;
	HotelView myView;
	
	public BulkUpdateHotelView(HotelView myView, HotelService service) {
		this.myView = myView;
		this.service = service;
		
		//hide fields for category and operatesFrom
		category.setVisible(false);
		operatesFrom.setVisible(false);
		
		//setup items to NativeSelects
		category.setItems(service.getCategoriesList());
		category.setItemCaptionGenerator(Category::getName);
		category.setPlaceholder("Please select category");
		category.setEmptySelectionAllowed(false);
		fields.setItems(Arrays.asList(Hotel.class.getDeclaredFields()));
		fields.setItemCaptionGenerator(Field::getName);
		fields.setPlaceholder("Please select field");
		fields.setEmptySelectionAllowed(false);
		
		//Text values design
		fields.setSizeFull();
		category.setSizeFull();
		newValue.setSizeFull();
		newValue.setPlaceholder("Field value");	
		setCaption("Bulk update");
		setContent(form);
		
		//add click listeners and style to buttons
		update.addClickListener(e -> updateField());
		update.setStyleName(ValoTheme.BUTTON_PRIMARY);
		cancel.addClickListener(e -> closePopup());
		cancel.setClickShortcut(KeyCode.ESCAPE);
		
		fields.addSelectionListener(e -> {
			//get field type if exist, otherwise get null
			Optional<Field> selectedField = e.getSelectedItem();
			String typeName = null;
			if (selectedField.isPresent()){
			Class<?> fieldType = selectedField.get().getType();
		    typeName = fieldType.getSimpleName();
		    }
			
			//set fields visibility 
			newValue.setVisible(typeName == null || typeName.equals("String") 
		    			                         || typeName.equals("Integer"));
			category.setVisible(typeName != null && typeName.equals("Category"));
			//you must change this if you add other Long field in the Hotel
			operatesFrom.setVisible(typeName != null && typeName.equals("Long"));
		 });
	}
	

	//method for field validating and updating
	private void updateField() {
		Optional<Field> select = fields.getSelectedItem();
		//if field is not selected -> view notification and not update
		if (select.isPresent()) {
			//get selected field's name and type
			Field selected = select.get();
			String fieldName = selected.getName();		
			selected.setAccessible(true);
			//get valid value for selected field
			Object validValue = getValidValue(fieldName);
			//update field for Hotel's set if valid value exist
			if(validValue != null){
			for(Hotel hotel:selectedHotels){
				try {
					selected.set(hotel, validValue);
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
				service.save(hotel);
			}	
			myView.updateHotelList();
			closePopup();
			}
		} else
			Notification.show("Please, select field before update");
	}
	
	//Validate value for selected field and return value if valid, and null and message if not valid
	private Object getValidValue(String fieldName){
		switch (fieldName) {
		//rating must be positive number [1,2,3,4,5]
		case "rating":
			String rating = newValue.getValue();
			if(rating.matches("[1-5]")){
				return Integer.parseInt(rating);
			} else {
				Notification.show("Rating must be a positive number from 1 to 5");
				return null;
			}
	    //hotel must work today
		case "operatesFrom":
			LocalDate date = operatesFrom.getValue();
			if(date == null){
				Notification.show("Please, input data");
			} else{
				Long duration = Duration.between(date.atTime(0, 0), LocalDate.now().atTime(0, 0)).toDays();
				if(duration >= 0){
					return duration;
				} else {
					Notification.show("Please, input data before today");
					return null;
					}	
			}
		//category cann't be null
		case "category":
			Optional<Category> categoryItem = category.getSelectedItem();
			if(categoryItem.isPresent()){
				return categoryItem.get();
			} else {
				Notification.show("Please, choose category before saving");
				return null;
			}
		//value for all String fields
		default:
			return newValue.getValue();
		}
	}

	//hide popup and reset all fields
	private void closePopup() {
	    resetFields();
	    setVisible(false);
	}
	
	private void resetFields() {
		fields.setSelectedItem(null);
	    category.clear();
	    operatesFrom.clear();
	    newValue.clear();
	}

	public void setHotels(Set<Hotel> selectedHotels){
		resetFields();
		this.selectedHotels = selectedHotels;
		setVisible(true);
	}
	
}
