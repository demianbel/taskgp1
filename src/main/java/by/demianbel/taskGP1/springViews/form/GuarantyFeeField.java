package by.demianbel.taskGP1.springViews.form;

import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.data.converter.StringToIntegerConverter;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomField;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.RadioButtonGroup;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import by.demianbel.taskGP1.backend.entity.GuarantyFee;

@SuppressWarnings("serial")
public class GuarantyFeeField extends CustomField<GuarantyFee> {

	private Label cashLabel = new Label("Payment will be made directly in the Hotel");
	private TextField feeField = new TextField();
	private RadioButtonGroup<String> select = new RadioButtonGroup<>();
	private GuarantyFee value = null;
	//create instance for binder
	private GuarantyFee bean = new GuarantyFee();
	private Binder<GuarantyFee> binder = new Binder<>(GuarantyFee.class);

	@Override
	public GuarantyFee getValue() {
        //custom field returns GuarantyFee object with any number for "Credit Card"
		//or null GuarantyFee object for "Cash"
		//GuarantyFee object with guarantyfee field == null is not valid value
		//you must check it in top level
		return value;
	}

	@Override
	protected Component initContent() {
		VerticalLayout field = new VerticalLayout();
		field.setMargin(false);
		cashLabel.setSizeFull();
		select.setItems("Credit Card", "Cash");
		select.setSizeFull();
		select.setSelectedItem("Cash");
		select.setStyleName(ValoTheme.OPTIONGROUP_HORIZONTAL);
		feeField.setPlaceholder("Guaranty Deposit");
		feeField.setVisible(false);
		feeField.setSizeFull();
		feeField.setDescription("Guaranty fee percent");

		//bind textfield to binder
		binder.forField(feeField).withConverter(
				       new StringToIntegerConverter(null, "Value must be positive number"))
		               .bind(GuarantyFee::getGuarantyFee, GuarantyFee::setGuarantyFee);

		
		binder.addValueChangeListener(e -> {
			//ignore Value change events from "Cash" case
			if (select.getSelectedItem().get().equals("Credit Card")) {
				//save valid value to instance
				//and customField's value to fire event
				boolean isValid = binder.isValid();
				if (isValid) {
					try {
						binder.writeBean(bean);
						setValue(new GuarantyFee(bean.getGuarantyFee()));
					} catch (ValidationException e1) {
						//we can't see it
						Notification.show("Kavabanga");
					}
				} else {
					//clear field if it contain not number symbols
					feeField.clear();
				}
			}
		});

		select.addSelectionListener(selected -> {
			String sel = selected.getValue();
			//set value to null for "Cash" case
			if (sel.equals("Cash")) {
				feeField.clear();
				setValue(null);
			} else {
				//input not valid value to fire event
				feeField.setValue("NotValidValue");
			}
			feeField.setVisible(sel.equals("Credit Card"));
			cashLabel.setVisible(sel.equals("Cash"));

		});

		field.addComponents(select, cashLabel, feeField);
		return field;
	}

	@Override
	protected void doSetValue(GuarantyFee newValue) {
		if (newValue == null) {
			select.setSelectedItem("Cash");
			value = null;
		} else {
			select.setSelectedItem("Credit Card");
			Integer feeValue = newValue.getGuarantyFee();
			value = new GuarantyFee(feeValue);
			feeField.setValue(feeValue == null ? "" : newValue.getGuarantyFee().toString());
		}
	}

}
