package by.demianbel.taskGP1.backend.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class GuarantyFee {
    
	@Column(name = "GUARANTY_FEE")
	private Integer guarantyFee;

	public Integer getGuarantyFee() {
		return guarantyFee;
	}

	public void setGuarantyFee(Integer guaranteeFee) {
		this.guarantyFee = guaranteeFee;
	}

	public GuarantyFee(Integer guarantyFee) {
		super();
		this.guarantyFee = guarantyFee;
	}

	public GuarantyFee() {
		super();
	}

	
	
}
