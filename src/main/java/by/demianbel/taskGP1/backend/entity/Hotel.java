package by.demianbel.taskGP1.backend.entity;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;


@SuppressWarnings("serial")
@Entity
@Table(name = "HOTEL")
@NamedQueries({
	@NamedQuery(name = "findAllHotelsFiltered",
			query = "SELECT h FROM Hotel h WHERE h.name LIKE :nameFilter AND h.address LIKE :addressFilter")
})
public class Hotel extends AbstractEntity{

	@NotNull(message = "name required")
	private String name;

	@NotNull(message = "address required")
	private String address;

	@NotNull(message = "rating required")
	private Integer rating;
	
	@Embedded
	private GuarantyFee guarantyFee;

	@NotNull(message = "operatesFrom required")
	@Column (name = "operates_from")
	private Long operatesFrom;

	@ManyToOne
	@JoinColumn(name = "category_id")
	private Category category;
	
	private String description;
	
	@NotNull(message = "url required")
	private String url;
	
	@Override
	public String toString() {
		return name + " " + rating +"stars " + address;
	}

	@Override
	protected Hotel clone() throws CloneNotSupportedException {
		return (Hotel) super.clone();
	}

	public Hotel() {
		this("", "", 1, 0L, null, "", "");
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	public GuarantyFee getGuarantyFee() {
		return guarantyFee;
	}

	public void setGuarantyFee(GuarantyFee guarantyFee) {
		this.guarantyFee = guarantyFee;
	}

	public Long getOperatesFrom() {
		return operatesFrom;
	}

	public void setOperatesFrom(Long operatesFrom) {
		this.operatesFrom = operatesFrom;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}	

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Hotel(String name, String address, Integer rating, Long operatesFrom, Category category, String description, String url) {
		this.name = name;
		this.address = address;
		this.rating = rating;
		this.operatesFrom = operatesFrom;
		this.category = category;
		this.description = description;
		this.url = url;
	}

}