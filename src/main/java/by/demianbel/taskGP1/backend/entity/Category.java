package by.demianbel.taskGP1.backend.entity;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@SuppressWarnings("serial")
@Entity
@Table(name = "CATEGORY")
@NamedQueries({
	@NamedQuery(name = "findAllCategories",
			query = "SELECT c FROM Category c")
})
public class Category extends AbstractEntity {
	
	@NotNull(message = "name can not be null")
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String category) {
		this.name = category;
	}

	public Category(String name) {
		this.name = name;
	}

	public Category() {
		this("");
	}
	
	@Override
	public String toString() {
		return name;
	}

}
