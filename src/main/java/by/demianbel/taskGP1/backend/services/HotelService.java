package by.demianbel.taskGP1.backend.services;

import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import by.demianbel.taskGP1.backend.entity.AbstractEntity;
import by.demianbel.taskGP1.backend.entity.Category;
import by.demianbel.taskGP1.backend.entity.Hotel;
@Repository
public class HotelService {

	//private static HotelService instance;
	private static final Logger LOGGER = Logger.getLogger(HotelService.class.getName());

	@PersistenceContext
	EntityManager em;
	
	public HotelService() {
	}
    
	//SELECT ALL HOTELS FROM HOTEL TABLE
	public List<Hotel> findAll() {
		return findAll("","");
	}

	//SELECT HOTELS FROM HOTEL TABLE WITH FILTERS
	@Transactional
	public List<Hotel> findAll(String nameFilter, String addressFilter) {
		Query query = em.createNamedQuery("findAllHotelsFiltered", Hotel.class);
		query.setParameter("nameFilter", "%" + nameFilter + "%");
		query.setParameter("addressFilter", "%" + addressFilter + "%");
		return query.getResultList();
	}
	
	//SELECT ALL CATEGORIES FROM CATEGORY TABLE
	@Transactional
	public List<Category> getCategoriesList() {
		Query query = em.createNamedQuery("findAllCategories", Category.class);
		return query.getResultList();
	}
    
	//SELECT HOTELS FROM HOTEL TABLE WITH FILTERS AND START AND MAXRESULT PARAMETERS
	@Transactional
	public List<Hotel> findAll(String nameFilter, String addressFilter, int start, int maxresults) {
		Query query = em.createNamedQuery("findAllHotelsFiltered", Hotel.class);
		query.setParameter("nameFilter", "%" + nameFilter + "%");
		query.setParameter("addressFilter", "%" + addressFilter + "%");
		query.setFirstResult(start);
		query.setMaxResults(maxresults);
		return query.getResultList();
	}

	//delete hotel from database
	@Transactional
	public <T extends AbstractEntity> void delete(T value) {
			//attach entity if it detached
			em.remove(em.contains(value) ? value : em.merge(value));
	}
	
	//remove selected categories from database
	@Transactional
	public void removeCategories(Set<Category> items) {
		Iterator<Category> itemsIterator = items.iterator();
		while (itemsIterator.hasNext()) {
			delete(itemsIterator.next());
		}

		//and refresh hotels removed categories -> null
		List<Hotel> hotelsList = findAll();
		for(Hotel hotel:hotelsList){
			em.refresh(hotel);
		}
	}

	//add entity if it doesn't exist and update entity if it exists
	@Transactional
	public <T extends AbstractEntity> void save(T entry) {
		if (entry == null) {
			LOGGER.log(Level.SEVERE, "Hotel is null.");
			return;
		}
			if (entry.getId() == null) {
				em.persist(entry);
			} else {
				em.merge(entry);
			}
	}

}
