package by.demianbel.taskGP1;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.annotations.Theme;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.navigator.SpringViewProvider;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import by.demianbel.taskGP1.springViews.CategoryView;
import by.demianbel.taskGP1.springViews.HotelView;

/**
 * This UI is the application entry point. A UI may either represent a browser window 
 * (or tab) or some part of a html page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be 
 * overridden to add component to the user interface and initialize non-component functionality.
 */
@Theme("mytheme")
@SpringUI
@SuppressWarnings("serial")
public class MyUI extends UI {

	@Autowired
    private SpringViewProvider viewProvider;
    
	@Override
    protected void init(VaadinRequest vaadinRequest) {
		//create main layout
		final VerticalLayout page = new VerticalLayout();
		
		//create component for SpringView
		final VerticalLayout viewComponent = new VerticalLayout();
		
		//create navigator for viewComponent
		Navigator navigator = new Navigator(this, viewComponent);
		navigator.addProvider(viewProvider);
    	
		//creating menu and two different menu items (for hotels and categories)
    	MenuBar menu = new MenuBar();
    	menu.addItem("Hotel List", item -> navigator.navigateTo(HotelView.VIEW_NAME));
    	menu.addItem("Categories List", item -> navigator.navigateTo(CategoryView.VIEW_NAME));    	
    	
    	//menu.setId("pageChangeMenu");
    	//setup start page
    	page.addComponents(menu, viewComponent);
    	navigator.navigateTo(HotelView.VIEW_NAME);
    	setContent(page);
            	
    }
}
