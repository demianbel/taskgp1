package by.demianbel.taskGP1.SpringConfiguration;

import java.beans.PropertyVetoException;
import java.sql.SQLException;

import javax.persistence.EntityManagerFactory;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.persistenceunit.DefaultPersistenceUnitManager;
import org.springframework.orm.jpa.support.SharedEntityManagerBean;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.vaadin.spring.annotation.EnableVaadin;

@Configuration
@ComponentScan("by.demianbel.taskGP1")
@EnableVaadin
@EnableTransactionManagement
public class MyConfiguration {
	
	@Bean(destroyMethod = "close")
	public ComboPooledDataSource dataSource() throws PropertyVetoException, SQLException{
		ComboPooledDataSource dataSource = new ComboPooledDataSource();
		dataSource.setDriverClass("com.mysql.jdbc.Driver");
		dataSource.setJdbcUrl("jdbc:mysql://localhost:3306/demo_hotels");
		dataSource.setUser("demo");
		dataSource.setPassword("demo");
		//pool properties
		dataSource.setMinPoolSize(20);
		dataSource.setMaxPoolSize(40);
		dataSource.setAcquireIncrement(1);
		dataSource.setMaxStatements(0);
		dataSource.setIdleConnectionTestPeriod(3000);
		dataSource.setLoginTimeout(300);
		return dataSource;
	}
	
	
	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() throws PropertyVetoException, SQLException{
		LocalContainerEntityManagerFactoryBean entFactory = new LocalContainerEntityManagerFactoryBean();
		entFactory.setDataSource(dataSource());
		entFactory.setPersistenceUnitName("hotel_demo");
		entFactory.setJpaDialect(new HibernateJpaDialect());
		entFactory.setJpaDialect(jpaDialect());
		entFactory.setJpaVendorAdapter(jpaVendorAdapter());
		entFactory.setPersistenceUnitManager(persistenceUnitManager());
		return entFactory;
	}
	
	@Bean
	public HibernateJpaDialect jpaDialect(){
		return new HibernateJpaDialect();
	}
	
	@Bean
	public HibernateJpaVendorAdapter jpaVendorAdapter(){
		HibernateJpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();
		jpaVendorAdapter.setShowSql(false);
		jpaVendorAdapter.setDatabasePlatform("org.hibernate.dialect.MySQLInnoDBDialect");
		return jpaVendorAdapter;
	}
	
	@Bean 
	public DefaultPersistenceUnitManager persistenceUnitManager() throws PropertyVetoException, SQLException{
		DefaultPersistenceUnitManager defaultPersistenceUnitManager = new DefaultPersistenceUnitManager();
		defaultPersistenceUnitManager.setDefaultDataSource(dataSource());
		defaultPersistenceUnitManager.setDefaultPersistenceUnitName("hotel_demo");
		return defaultPersistenceUnitManager;
	}
	
	@Bean
	public SharedEntityManagerBean em(EntityManagerFactory emf) throws PropertyVetoException, SQLException{
		SharedEntityManagerBean entityManager = new SharedEntityManagerBean();
		entityManager.setEntityManagerFactory(emf);
		entityManager.setPersistenceUnitName("hotel_demo");
		return entityManager;
	}
	
	@Bean
	public JpaTransactionManager transactionManager(EntityManagerFactory emf) throws PropertyVetoException, SQLException{
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(emf);
		return transactionManager;
	}
	
}
